# ML PROJECT BACKBONE <img src="https://raw.githubusercontent.com/MartinHeinz/MartinHeinz/master/wave.gif" width="30px">

En este repositorio, proponemos una estrucura de directorio para modularizar un proyecto de ML.

## Clone repository

```github
git clone https://gitlab.com/rcoronado/ml-project-backbone.git
```

## Simple Build Docker Image :whale:

### Build Trainer Task

```bash
docker build -f dockerfiles/Dockerfile.trainer -t project-demo:trainer-v1 .
```

### Build Predictor Task

```bash
docker build -f dockerfiles/Dockerfile.predictor -t project-demo:predictor-v1 .
```

### Get Image ID

```bash
docker images
```

```console
# OUTPUT
REPOSITORY       TAG            IMAGE ID       CREATED       SIZE
project-demo    predictor-v1   f3c308f80c7a   2 hours ago   1.22GB
project-demo    trainer-v1     2c6de70230a6   2 hours ago   1.22GB
...             ...            ...            ...           ...
```

### Run Image

```bash
# Run Test
docker run [IMAGE ID]

# Run with params
docker run [IMAGE ID] --arg1 Test1 --arg2 Test2

# Run image and access shell container
docker run -it --entrypoint /bin/bash [container-id]
```

---

## Build Docker Image with Volume

### Create Docker Volume

```bash
docker volume create [volume-name]
docker volume ls
docker volume inspect [volume-name]
```

```console
# OUTPUT
[
    {
        "CreatedAt": "2020-09-28T00:02:01Z",
        "Driver": "local",
        "Labels": {},
        "Mountpoint": "/var/lib/docker/volumes/data/_data",
        "Name": "data",
        "Options": {},
        "Scope": "local"
    }
]
```

### Run Image and Mount Volume

```bash
# Run Test
docker run --mount source=[volume-name],destination=/app/experiments [IMAGE ID]

# Run with params
docker run --mount source=[volume-name],destination=/app/experiments [IMAGE ID] --arg1 Test1 --arg2 Test2

# Run image and access shell container
docker run -it --entrypoint /bin/bash --mount source=[volume-name],destination=/app/experiments [IMAGE ID]
```

## List Docker Containers

```bash
docker ps -a
```

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[MIT](https://choosealicense.com/licenses/mit/)
