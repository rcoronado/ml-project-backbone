# SET PATH MODULES PROJECT
import os
import sys
src_dir = os.path.join(os.getcwd(), '..')
sys.path.append(src_dir)

# LOGGING
from d00_utils.logger import get_logger, get_inspect
logger = get_logger(__name__)

# IMPORTs
from variables import D05_EVALUATIONS
import pandas as pd
from d04_modeling.index import score_model_sklearn


# EXPERIMENT SAVE EVALUATIONS
def experiment_save_evaluations(storage, models, x_data, y_data):
    logger.info(get_inspect())

    new_data = []
    for idx, item in enumerate(models):
        name = item['name']
        model = item['model']
        score = score_model_sklearn(model, x_data, y_data)
        new_data.append({
            'filename': name,
            'score': score
        })
    storage.register(type_step=D05_EVALUATIONS, name='metrics', data=new_data)
    return new_data


def metrics_models(names, metrics):
    logger.info(get_inspect())

    results = pd.DataFrame({'Model': names, 'Score': metrics})
    result_df = results.sort_values(by='Score', ascending=False)
    result_df = result_df.set_index('Score')
    return result_df

