# SET PATH MODULES PROJECT
import os
import sys
src_dir = os.path.join(os.getcwd(), '..')
sys.path.append(src_dir)

# LOGGING
from d00_utils.logger import get_logger, get_inspect
logger = get_logger(__name__)

# DECORATORS
from d00_utils.decorators import timer, save_model

# IMPORTS
from d00_utils.save_models import sklearn_load_model, sklearn_save_model
from variables import (
    MODEL_TYPE_SKLEARN, MODEL_TYPE_TENSORFLOW, MODEL_TYPE_XGBOOST, PATH_EXP_MODELS,
    D04_MODELS, D06_RESULTS, PATH_EXP_RESULTS
)
from d00_utils.utils import generate_datetime_name

import numpy as np
import pandas as pd
from sklearn import linear_model
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import Perceptron
from sklearn.linear_model import SGDClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.naive_bayes import GaussianNB

# EXPERIMENT SAVE MODEL
def experiment_save_best_model(storage, models, x_data, y_data):
    logger.info(get_inspect())

    path_save = PATH_EXP_MODELS.format(storage.id)
    os.makedirs(path_save, exist_ok=True)

    best_model = None
    best_score = 0
    for idx, item in enumerate(models):
        name = item['name']
        model = item['model']
        score = score_model_sklearn(model, x_data, y_data)
        if score >= best_score:
            best_score = score
            best_model = model
    

    model_name = '{}.{}'.format(generate_datetime_name(), 'sav')
    path_save_model = os.path.join(path_save, model_name)
    sklearn_save_model(model, path_save_model)
    
    new_data = {
        'filename': model_name,
        'path': path_save_model,
        'score': best_score
    }
    storage.register(type_step=D04_MODELS, name='best_model', data=new_data)


# EXPERIMENT SAVE PREDICT
def experiment_save_predict(storage, result, data):
    logger.info(get_inspect())
    filename = '{}.{}'.format(generate_datetime_name(), 'csv')
    path_save = PATH_EXP_RESULTS.format(storage.id)
    os.makedirs(path_save, exist_ok=True)
    path_save = os.path.join(path_save, filename)
    result.to_csv(path_save, index=False, header=True)
    storage.register(type_step=D06_RESULTS, name='Predict', path_file=path_save, data=data)
    

# SCORE MODEL SKLEARN
def score_model_sklearn(model, x_data, y_data):
    return round(model.score(x_data, y_data) * 100, 2)

# PREDICT MODEL SKLEARN
def predict_model_sklearn(model, x_data, y_data, path_model=None):
    local_model = model
    if not local_model:
        local_model = sklearn_load_model(path_model)
    try:
        return local_model.predict(x_data)
    except Exception as e:
        logger.error(str(e))
        return None



# RANDOM FOREST
@timer
# @save_model(None, MODEL_TYPE_SKLEARN)
def trainer_random_forest(train_df, train_labels):
    logger.info(get_inspect())

    random_forest = RandomForestClassifier(n_estimators=100)
    random_forest.fit(train_df, train_labels)
    return random_forest


# LOGISTIC REGRESSION
@timer
def trainer_logistic_regression(train_df, train_labels):
    logger.info(get_inspect())

    logreg = LogisticRegression(solver='lbfgs', max_iter=110)
    logreg.fit(train_df, train_labels)
    # return round(logreg.score(train_df, train_labels) * 100, 2)
    return logreg


# GAUSSIAN
@timer
def trainer_gaussian_nb(train_df, train_labels):
    logger.info(get_inspect())

    gaussian = GaussianNB()
    gaussian.fit(train_df, train_labels)
    # return round(gaussian.score(train_df, train_labels) * 100, 2)
    return gaussian


# LINEAR SVC
@timer
def trainer_linear_svc(train_df, train_labels):
    logger.info(get_inspect())

    linear_svc = SVC(gamma='auto')
    linear_svc.fit(train_df, train_labels)
    # return round(linear_svc.score(train_df, train_labels) * 100, 2)
    return linear_svc


# DECISION TREE
@timer
def trainner_decision_tree(train_df, train_labels):
    logger.info(get_inspect())

    decision_tree = DecisionTreeClassifier()
    decision_tree.fit(train_df, train_labels)
    # return round(decision_tree.score(train_df, train_labels) * 100, 2)
    return decision_tree