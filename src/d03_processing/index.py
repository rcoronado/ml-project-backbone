# SET PATH MODULES PROJECT
import os
import sys
src_dir = os.path.join(os.getcwd(), '..')
sys.path.append(src_dir)

# LOGGING
from variables import PATH_EXP_INTERMEDIATE, D03_PROCESSED
from d00_utils.logger import get_logger, get_inspect
logger = get_logger(__name__)

# IMPORTS
import numpy as np
import pandas as pd
import re

# EXPERIMENT SAVE PROCESSING
def experiment_save_processing(storage, data, params=None):
    logger.info(get_inspect())

    path_save = PATH_EXP_INTERMEDIATE.format(storage.id)
    os.makedirs(path_save, exist_ok=True)

    new_data = []
    for item in data:
        filename = item['filename']
        dataframe = item['dataframe']
        path = os.path.join(path_save, filename)
        try:
            dataframe.to_csv(path, index=False, header=True)
            new_data.append({
                'filename': filename,
                'path': path
            })
        except:
            pass
    
    storage.register(type_step=D03_PROCESSED, name='', params=params, data=new_data)


def split_data(train_df, test_df):
    logger.info(get_inspect())

    PREDICTION_LABEL = 'Survived'
    PASSENGER_ID = 'PassengerId'

    train_labels = train_df[PREDICTION_LABEL]
    train_df = train_df.drop(PREDICTION_LABEL, axis=1)
    test_df = test_df.drop(PASSENGER_ID, axis=1)
    return (train_df, train_labels), (test_df, None)


def preprocessing_data(train_df, test_df):
    logger.info(get_inspect())

    train_df, test_df = step1_combine_sibsp_parch(train_df, test_df)
    train_df, test_df = step2_missing_data_cabin(train_df, test_df)
    train_df, test_df = step3_missing_data_age(train_df, test_df)
    train_df, test_df = step4_missing_data_embarked(train_df, test_df)
    train_df, test_df = step5_convert_features(train_df, test_df)
    train_df, test_df = step6_title_features(train_df, test_df)
    train_df, test_df = step7_sex_in_numeric_and_drop_ticket(train_df, test_df)
    train_df, test_df = step8_embarked_in_numeric(train_df, test_df)
    train_df, test_df = step9_age_in_categries(train_df, test_df)
    train_df, test_df = step10_fare_in_categories(train_df, test_df)
    train_df, test_df = step11_nf_age_times(train_df, test_df)
    train_df, test_df = step12_fare_per_person(train_df, test_df)
    return train_df, test_df


def step1_combine_sibsp_parch(train_df, test_df):
    data = [train_df, test_df]
    for dataset in data:
        dataset['relatives'] = dataset['SibSp'] + dataset['Parch']
        dataset.loc[dataset['relatives'] > 0, 'not_alone'] = 0
        dataset.loc[dataset['relatives'] == 0, 'not_alone'] = 1
        dataset['not_alone'] = dataset['not_alone'].astype(int)
    
    train_df = train_df.drop(['PassengerId'], axis=1)
    return train_df, test_df


def step2_missing_data_cabin(train_df, test_df):
    deck = {"A": 1, "B": 2, "C": 3, "D": 4, "E": 5, "F": 6, "G": 7, "U": 8}
    data = [train_df, test_df]

    for dataset in data:
        dataset['Cabin'] = dataset['Cabin'].fillna("U0")
        dataset['Deck'] = dataset['Cabin'].map(lambda x: re.compile("([a-zA-Z]+)").search(x).group())
        dataset['Deck'] = dataset['Deck'].map(deck)
        dataset['Deck'] = dataset['Deck'].fillna(0)
        dataset['Deck'] = dataset['Deck'].astype(int)
    # we can now drop the cabin feature
    train_df = train_df.drop(['Cabin'], axis=1)
    test_df = test_df.drop(['Cabin'], axis=1)
    return train_df, test_df


def step3_missing_data_age(train_df, test_df):
    data = [train_df, test_df]

    for dataset in data:
        mean = train_df["Age"].mean()
        std = test_df["Age"].std()
        is_null = dataset["Age"].isnull().sum()
        # compute random numbers between the mean, std and is_null
        rand_age = np.random.randint(mean - std, mean + std, size = is_null)
        # fill NaN values in Age column with random values generated
        age_slice = dataset["Age"].copy()
        age_slice[np.isnan(age_slice)] = rand_age
        dataset["Age"] = age_slice
        dataset["Age"] = train_df["Age"].astype(int)
    return train_df, test_df


def step4_missing_data_embarked(train_df, test_df):
    # fill with most common value
    common_value = 'S'
    data = [train_df, test_df]

    for dataset in data:
        dataset['Embarked'] = dataset['Embarked'].fillna(common_value)
    return train_df, test_df
    

def step5_convert_features(train_df, test_df):
    data = [train_df, test_df]
    for dataset in data:
        dataset['Fare'] = dataset['Fare'].fillna(0)
        dataset['Fare'] = dataset['Fare'].astype(int)
    return train_df, test_df


def step6_title_features(train_df, test_df):
    data = [train_df, test_df]
    titles = {"Mr": 1, "Miss": 2, "Mrs": 3, "Master": 4, "Rare": 5}

    for dataset in data:
        # extract titles
        dataset['Title'] = dataset.Name.str.extract(' ([A-Za-z]+)\.', expand=False)
        # replace titles with a more common title or as Rare
        dataset['Title'] = dataset['Title'].replace(['Lady', 'Countess','Capt', 'Col','Don', 'Dr',\
                                                'Major', 'Rev', 'Sir', 'Jonkheer', 'Dona'], 'Rare')
        dataset['Title'] = dataset['Title'].replace('Mlle', 'Miss')
        dataset['Title'] = dataset['Title'].replace('Ms', 'Miss')
        dataset['Title'] = dataset['Title'].replace('Mme', 'Mrs')
        # convert titles into numbers
        dataset['Title'] = dataset['Title'].map(titles)
        # filling NaN with 0, to get safe
        dataset['Title'] = dataset['Title'].fillna(0)
    train_df = train_df.drop(['Name'], axis=1)
    test_df = test_df.drop(['Name'], axis=1)
    return train_df, test_df


def step7_sex_in_numeric_and_drop_ticket(train_df, test_df):
    genders = {"male": 0, "female": 1}
    data = [train_df, test_df]

    for dataset in data:
        dataset['Sex'] = dataset['Sex'].map(genders)
    
    train_df = train_df.drop(['Ticket'], axis=1)
    test_df = test_df.drop(['Ticket'], axis=1)
    return train_df, test_df


def step8_embarked_in_numeric(train_df, test_df):
    ports = {"S": 0, "C": 1, "Q": 2}
    data = [train_df, test_df]

    for dataset in data:
        dataset['Embarked'] = dataset['Embarked'].map(ports)
    
    return train_df, test_df


def step9_age_in_categries(train_df, test_df):
    data = [train_df, test_df]
    for dataset in data:
        dataset['Age'] = dataset['Age'].astype(int)
        dataset.loc[ dataset['Age'] <= 11, 'Age'] = 0
        dataset.loc[(dataset['Age'] > 11) & (dataset['Age'] <= 18), 'Age'] = 1
        dataset.loc[(dataset['Age'] > 18) & (dataset['Age'] <= 22), 'Age'] = 2
        dataset.loc[(dataset['Age'] > 22) & (dataset['Age'] <= 27), 'Age'] = 3
        dataset.loc[(dataset['Age'] > 27) & (dataset['Age'] <= 33), 'Age'] = 4
        dataset.loc[(dataset['Age'] > 33) & (dataset['Age'] <= 40), 'Age'] = 5
        dataset.loc[(dataset['Age'] > 40) & (dataset['Age'] <= 66), 'Age'] = 6
        dataset.loc[ dataset['Age'] > 66, 'Age'] = 6
    
    return train_df, test_df


def step10_fare_in_categories(train_df, test_df):
    data = [train_df, test_df]

    for dataset in data:
        dataset.loc[ dataset['Fare'] <= 7.91, 'Fare'] = 0
        dataset.loc[(dataset['Fare'] > 7.91) & (dataset['Fare'] <= 14.454), 'Fare'] = 1
        dataset.loc[(dataset['Fare'] > 14.454) & (dataset['Fare'] <= 31), 'Fare']   = 2
        dataset.loc[(dataset['Fare'] > 31) & (dataset['Fare'] <= 99), 'Fare']   = 3
        dataset.loc[(dataset['Fare'] > 99) & (dataset['Fare'] <= 250), 'Fare']   = 4
        dataset.loc[ dataset['Fare'] > 250, 'Fare'] = 5
        dataset['Fare'] = dataset['Fare'].astype(int)

    return train_df, test_df


def step11_nf_age_times(train_df, test_df):
    data = [train_df, test_df]
    for dataset in data:
        dataset['Age_Class']= dataset['Age']* dataset['Pclass']
    return train_df, test_df


def step12_fare_per_person(train_df, test_df):
    data = [train_df, test_df]
    for dataset in data:
        dataset['Fare_Per_Person'] = dataset['Fare']/(dataset['relatives']+1)
        dataset['Fare_Per_Person'] = dataset['Fare_Per_Person'].astype(int)
    
    return train_df, test_df