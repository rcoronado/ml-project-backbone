import os 
import sys

# PATH_PROJECT
PATH_PROJECT = os.path.join(os.getcwd(), '..') 

# PATH_DATA
D01_RAW = 'd01_raw'
D02_INTERMEDIATE = 'd02_intermediate'
D03_PROCESSED = 'd03_processed'
D04_MODELS = 'd04_models'
D05_EVALUATIONS = 'd05_evaluations'
D06_RESULTS = 'd06_results'

PATH_DATA               = os.path.join(PATH_PROJECT, 'data')
PATH_DATA_RAW           = os.path.join(PATH_DATA, D01_RAW)
PATH_DATA_INTERMEDIATE  = os.path.join(PATH_DATA, D02_INTERMEDIATE)
PATH_DATA_PROCESSED     = os.path.join(PATH_DATA, D03_PROCESSED)
PATH_DATA_MODELS        = os.path.join(PATH_DATA, D04_MODELS)
PATH_DATA_EVALUATIONS   = os.path.join(PATH_DATA, D05_EVALUATIONS)
PATH_DATA_RESULTS   = os.path.join(PATH_DATA, D06_RESULTS)

# PATH_SRC
_D00_UTILS = 'd00_utils'
_D01_DATA = 'd01_data'
_D02_INTERMEDIATE = 'd02_intermediate'
_D03_PROCESSING = 'd03_processing'
_D04_MODELING = 'd04_modeling'
_D05_EVALUATIONS = 'd05_evaluations'
_D06_RESULTS = 'd06_results'

PATH_SRC                = os.path.join(PATH_PROJECT, 'src')
PATH_SRC_UTILS          = os.path.join(PATH_SRC, _D00_UTILS)
PATH_SRC_DATA           = os.path.join(PATH_SRC, _D01_DATA)
PATH_SRC_INTERMEDIATE   = os.path.join(PATH_SRC, _D02_INTERMEDIATE)
PATH_SRC_PROCESSING     = os.path.join(PATH_SRC, _D03_PROCESSING)
PATH_SRC_MODELING       = os.path.join(PATH_SRC, _D04_MODELING)
PATH_SRC_EVALUATIONS    = os.path.join(PATH_SRC, _D05_EVALUATIONS)
PATH_SRC_RESULTS      = os.path.join(PATH_SRC, _D06_RESULTS)

# PATH_EXPERIMENTS
PATH_EXP               = os.path.join(PATH_PROJECT, 'experiments')
PATH_EXP_RAW           = os.path.join(PATH_EXP, '{}', D01_RAW)
PATH_EXP_INTERMEDIATE  = os.path.join(PATH_EXP, '{}', D02_INTERMEDIATE)
PATH_EXP_PROCESSED     = os.path.join(PATH_EXP, '{}', D03_PROCESSED)
PATH_EXP_MODELS        = os.path.join(PATH_EXP, '{}', D04_MODELS)
PATH_EXP_EVALUATIONS   = os.path.join(PATH_EXP, '{}', D05_EVALUATIONS)
PATH_EXP_RESULTS       = os.path.join(PATH_EXP, '{}', D06_RESULTS)

# MODEL
MODEL_TYPE_SKLEARN = 'sklearn'
MODEL_TYPE_TENSORFLOW = 'tensorflow'
MODEL_TYPE_XGBOOST = 'xgboost'

# TASK TYPE
TRAINER = 'trainer'
PREDICTOR = 'predictor'
DEPLOY = 'deploy'
