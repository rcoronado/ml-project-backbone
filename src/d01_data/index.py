# SET PATH MODULES PROJECT
import os
import sys
src_dir = os.path.join(os.getcwd(), '..')
sys.path.append(src_dir)

# LOGGING
from d00_utils.logger import get_logger, get_inspect
logger = get_logger(__name__)

# DECORATORS
from d00_utils.decorators import timer

# IMPORTS
from variables import PATH_DATA_RAW
import pandas as pd

# FUNCTIONS

@timer
def load_raw_data(train_file, test_file):
    logger.info(get_inspect())

    path_data = PATH_DATA_RAW
    test_df = pd.read_csv(os.path.join(path_data, test_file))
    train_df = pd.read_csv(os.path.join(path_data, train_file))

    return train_df, test_df