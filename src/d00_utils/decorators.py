# SET PATH MODULES PROJECT
import os
import sys
src_dir = os.path.join(os.getcwd(), '..')
sys.path.append(src_dir)

# LOGGING
from d00_utils.logger import get_logger, get_inspect
logger = get_logger(__name__)

# DECORATORS
from d00_utils.save_models import sklearn_save_model
from d00_utils.utils import generate_datetime_name

from variables import (PATH_DATA_MODELS, PATH_EXP_MODELS, MODEL_TYPE_SKLEARN, MODEL_TYPE_TENSORFLOW, MODEL_TYPE_XGBOOST)
import functools
import time

def timer(func):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        start_time = time.perf_counter()
        value = func(*args, **kwargs)
        end_time = time.perf_counter()
        run_time = end_time - start_time
        logger.warning("Finished {} in {} secs".format(repr(func.__name__), round(run_time, 3)))
        return value
    return wrapper


def save_model(*args_, **kwargs_):
    def inner_function(func):
        @functools.wraps(func)
        def wrapper(*args, **kwargs):
            experiment_id = args_[0]
            model_type = args_[1]
            path_save = PATH_EXP_MODELS.format(experiment_id) if experiment_id else PATH_DATA_MODELS
            path_save_model = None
            model = func(*args, **kwargs)
            if MODEL_TYPE_SKLEARN == model_type:
                model_name = '{}.{}'.format(generate_datetime_name(), 'sav')
                path_save_model = os.path.join(path_save, model_name)
                sklearn_save_model(model, path_save_model)
            
            elif MODEL_TYPE_TENSORFLOW == model_type:
                pass
            
            elif MODEL_TYPE_XGBOOST == model_type:
                pass
                
            else:
                custom_func = args_[2]
                path_save_model = custom_func(model, path_save)

            return model, path_save_model
        return wrapper

    return inner_function