from datetime import datetime, timedelta
from pytz import timezone

TZ = timezone('America/lima')
NOW = datetime.now(TZ)


def generate_datetime_now(format_date=None):
    return datetime.now(TZ).strftime(format_date) if format_date else datetime.now(TZ)

def generate_datetime_name():
    return generate_datetime_now('%Y%m%d%H%M%S')

def generate_datetime_created():
    return generate_datetime_now('%Y-%m-%d %H:%M:%S')

def generate_timestamp():
    now = generate_datetime_now()
    timestamp =  datetime.timestamp(now)
    # return ''.join(str(timestamp).split('.'))
    return str(timestamp).split('.')[0]
