# SET PATH MODULES PROJECT
import os
import sys
src_dir = os.path.join(os.getcwd(), '..')
sys.path.append(src_dir)
# IMPORTS
from variables import PATH_PROJECT

import inspect
import logging
import coloredlogs
from logging.handlers import TimedRotatingFileHandler, RotatingFileHandler
FORMATTER = logging.Formatter("%(asctime)s — %(name)s — %(levelname)s — %(message)s")
LOG_FILE =  os.path.join(PATH_PROJECT, "logging.log")

def get_console_handler():
    console_handler = logging.StreamHandler(sys.stdout)
    console_handler.setFormatter(FORMATTER)
    return console_handler

def get_file_handler():
    # file_handler = TimedRotatingFileHandler(LOG_FILE, when='midnight')
    file_handler = RotatingFileHandler(LOG_FILE)
    file_handler.setFormatter(FORMATTER)
    return file_handler

def get_logger(logger_name):
    logger = logging.getLogger(logger_name)
    logger.setLevel(logging.DEBUG) # better to have too much log than not enough
    logger.addHandler(get_console_handler())
    logger.addHandler(get_file_handler())
    # with this pattern, it's rarely necessary to propagate the error up to parent
    logger.propagate = False
    coloredlogs.install(
        level=logging.DEBUG, 
        logger=logger,
        fmt='%(asctime)s,%(msecs)03d %(hostname)s %(name)s[%(lineno)d] %(levelname)s %(message)s'
    )
    return logger

def get_inspect():
    # print(inspect.stack()[0][3])
    # print(inspect.stack()[1][3])
    # print(inspect.stack()[2][3])
    return inspect.stack()[1][3]