# SET PATH MODULES PROJECT
import os
import sys
src_dir = os.path.join(os.getcwd(), '..')
sys.path.append(src_dir)

# LOGGING
from variables import PATH_DATA_MODELS, PATH_EXP_MODELS
from d00_utils.logger import get_logger, get_inspect
logger = get_logger(__name__)

try:
    from sklearn.externals import joblib
except Exception as e:
    logger.error(e)


# TENSORFLOW


# XGBOOST


# SKLEARN


def sklearn_save_model(model, path_save):
    try:
        joblib.dump(model, path_save)
    except Exception as e:
        logger.error(e)
    return path_save

def sklearn_load_model(path_model):
    try:
        joblib_model = joblib.load(path_model)
    except Exception as e:
        joblib_model = None
        logger.error(e)
    return joblib_model