# SET PATH MODULES PROJECT
import os
import sys
src_dir = os.path.join(os.getcwd(), '..')
sys.path.append(src_dir)

# IMPORTS
from variables import PATH_PROJECT, D01_RAW, D02_INTERMEDIATE, D04_MODELS, D05_EVALUATIONS, PATH_EXP
from d00_utils.utils import generate_timestamp, generate_datetime_created

import numpy as np
import pandas as pd

COLUMNS_DATAFRAME = ['id','date', 'type_step', 'name', 'path_file', 'params', 'data']
FILE_METADATA = 'logger.json'

class ManagerModels:
    def __init__(self):
        self.path = None
        self.logger = None
        self.generate_id()
    
    def generate_id(self):
        self.id = generate_timestamp()
        self.path_storage = os.path.join(PATH_EXP, self.id)
        os.makedirs(self.path_storage, exist_ok=True)
    
    def create_logger(self, path_project=''):
        self.path = os.path.join(path_project, FILE_METADATA)
        try:
            self.logger = pd.read_json(self.path, dtype={'id': str, 'date': str})
        except:
            self.logger = pd.DataFrame(data=None, columns=COLUMNS_DATAFRAME)

    def register(self, type_step, name='', params={}, data={}, path_file=None):
        date = generate_datetime_created()
        jsonLog = { 
            'id': self.id, 'date': date, 'type_step': type_step, 'name': name,
            'params': params, 'data': data, 'path_file': path_file
        }
        self.logger = self.logger.append(jsonLog, ignore_index=True)
        self.logger.to_json(self.path)
    
    def get_last_experiment(self, experiment_step):
        registers = self.logger[self.logger['type_step']==experiment_step]
        registers = registers.sort_values(by='date', ascending=False)
        return registers.iloc[0].to_dict() if len(registers) > 0 else None

