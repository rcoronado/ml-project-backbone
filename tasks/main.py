from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

# SET PATH MODULES PROJECT
import os
import sys
path_dir = os.path.join(os.getcwd(), '..')
sys.path.append(path_dir)

# IMPORTS
import argparse
import logging
import tempfile
import os
import shutil

# TASK
from src.variables import TRAINER, PREDICTOR, DEPLOY
from tasks.trainer import execute_trainer
from tasks.predictor import execute_predictor

def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--task',
        help='GCS file or local paths to training data',
        default=TRAINER)
    parser.add_argument(
        '--train-files',
        help='GCS file or local paths to training data',
        default="train.csv")
    parser.add_argument(
        '--test-files',
        help='GCS file or local paths to evaluation data',
        default="test.csv")
    parser.add_argument(
        '--job-dir',
        type=str,
        help='Local or GCS location for writing checkpoints and exporting '
             'models')
    
    args, _ = parser.parse_known_args()
    return args


if __name__ == '__main__':
    args = get_args()

    task_condition = args.task
    if task_condition == TRAINER:
        execute_trainer(args)
    elif task_condition == PREDICTOR:
        execute_predictor(args)
    else:
        print('The task condition [{}] is not defined.'.format(task_condition))
        