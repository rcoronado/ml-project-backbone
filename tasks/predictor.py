# SET PATH MODULES PROJECT
import os
import sys
src_dir = os.path.join(os.getcwd(), '..', 'src')
sys.path.append(src_dir)

# LOGGING
from d00_utils.logger import get_logger, get_inspect
logger = get_logger(__name__)

# IMPORTS
from variables import PATH_DATA_RAW, PATH_EXP, D01_RAW, D02_INTERMEDIATE, D04_MODELS, D05_EVALUATIONS
from d00_utils.manager import ManagerModels
from d01_data.index import load_raw_data
from d00_utils.save_models import sklearn_load_model
from d03_processing.index import preprocessing_data, split_data
from d04_modeling.index import experiment_save_predict

import pandas as pd
import numpy as np

def task_predictor(path_train, path_test):
    logger.info('INIT_PREDICTOR')
    logger.info(get_inspect())

    # MANAGER TASK
    store = ManagerModels()
    store.create_logger(PATH_EXP)

    # LOAD RAW DATA
    train_df, test_df = load_raw_data(path_train, path_test)
    # PREPROCESSING DATA
    train_df, test_df = preprocessing_data(train_df, test_df)
    ## SPLIT DATA
    (x_train, y_train), (x_test, y_test) = split_data(train_df, test_df)

    # RECOVER MODEL
    recover = store.get_last_experiment(D04_MODELS)

    # LOAD MODEL
    path_best_model = recover['data']['path']
    model = sklearn_load_model(path_best_model)

    # PREDICT
    result = model.predict(x_test)

    # SAVE PREDICT
    x_test['Survived'] = result
    experiment_save_predict(store, x_test, recover)


def execute_predictor(args):
    # ARGUMENTS
    train_files = args.train_files
    test_files = args.test_files
    # TASK
    task_predictor(train_files, test_files)
