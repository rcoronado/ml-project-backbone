# SET PATH MODULES PROJECT
import os
import sys
src_dir = os.path.join(os.getcwd(), '..', 'src')
sys.path.append(src_dir)

# LOGGING
from d00_utils.logger import get_logger, get_inspect
logger = get_logger(__name__)


# IMPORTS
from variables import PATH_DATA_MODELS, PATH_EXP, D01_RAW, D02_INTERMEDIATE, D04_MODELS, D05_EVALUATIONS
from d00_utils.save_models import sklearn_load_model
from d00_utils.utils import generate_datetime_created, generate_timestamp
from d00_utils.manager import ManagerModels
from d01_data.index import load_raw_data
from d03_processing.index import (preprocessing_data, split_data, experiment_save_processing)
from d04_modeling.index import (
    trainer_gaussian_nb, trainer_linear_svc, trainer_logistic_regression,
    trainer_random_forest, trainner_decision_tree, score_model_sklearn, 
    predict_model_sklearn, experiment_save_best_model
)
from d05_evaluations.index import ( metrics_models, experiment_save_evaluations )


def task_trainer(path_train, path_test):
    logger.info('INIT_TRAINER')
    logger.info(get_inspect())

    # MANAGER TASK
    store = ManagerModels()
    store.create_logger(PATH_EXP)

    # LOAD RAW DATA
    train_df, test_df = load_raw_data(path_train, path_test)

    # PREPROCESSING DATA
    train_df, test_df = preprocessing_data(train_df, test_df)

    ## SPLIT DATA
    (x_train, y_train), (x_test, y_test) = split_data(train_df, test_df)
    ## SAVE PREPROCESSING
    data = [
        {'filename': 'x_train.csv', 'dataframe': x_train},
        {'filename': 'y_train.csv', 'dataframe': y_train},
        {'filename': 'x_test.csv', 'dataframe': x_test},
        {'filename': 'y_test.csv', 'dataframe': y_test},
    ]
    experiment_save_processing(store, data)

    # MODELING
    random_forest = trainer_random_forest(x_train, y_train)
    score_random_forest = score_model_sklearn(random_forest, x_train, y_train)

    logreg = trainer_logistic_regression(x_train, y_train)
    score_logreg = score_model_sklearn(logreg, x_train, y_train)

    gaussian = trainer_gaussian_nb(x_train, y_train)
    score_gaussian = score_model_sklearn(gaussian, x_train, y_train)

    linear_svc = trainer_linear_svc(x_train, y_train)
    score_linear_svc = score_model_sklearn(linear_svc, x_train, y_train)

    decision_tree = trainner_decision_tree(x_train, y_train)
    score_decision_tree = score_model_sklearn(decision_tree, x_train, y_train)

    ## SAVE MODELING
    data = [
        {'name': 'Support Vector Machines', 'model': linear_svc},
        {'name': 'Logistic Regression', 'model': logreg},
        {'name': 'Random Forest', 'model': random_forest},
        {'name': 'Naive Bayes', 'model': gaussian},
        {'name': 'Decision Tree', 'model': decision_tree}
    ]
    experiment_save_best_model(store, data, x_train, y_train)

    # METRICS
    metrics = experiment_save_evaluations(store, data, x_train, y_train)
    print(metrics)


def execute_trainer(args):
    # ARGUMENTS
    train_files = args.train_files
    test_files = args.test_files
    # TASK
    task_trainer(train_files, test_files)